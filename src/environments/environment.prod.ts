export const environment = {
  production: true
};

export const wmtslink = '/cocodati-server/api/config/1/getwmts/';

export const links = {
  getConfig: '/cocodati-server/api/config/1/getconfig/',
  getTestcases: '/cocodati-server/api/config/1/gettestcases/',
  getLanguageConfig: '/cocodati-server/api/config/',
  postFile: '/cocodati-server/api/convertfile/1/postfile/',
  convertCoords:'/cocodati-server/api/convert/1/convertCoords/'
};