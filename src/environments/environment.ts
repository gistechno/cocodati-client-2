// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const wmtslink = 'http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/getwmts/';
//export const wmtslink = 'https://gis.lmi.is/geoserver/gwc/service/wmts?service=WMTS&request=GetCapabilities';

export const links = {
  getConfig: 'http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/getconfig/',
  getTestcases: 'http://jardbakki.lmi.is:8088/cocodati-server/api/config/1/gettestcases/',
  getLanguageConfig: 'http://jardbakki.lmi.is:8088/cocodati-server/api/config/',
  postFile: 'http://jardbakki.lmi.is:8088/cocodati-server/api/convertfile/1/postfile/',
  convertCoords:'http://jardbakki.lmi.is:8088/cocodati-server/api/convert/1/convertCoords/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
