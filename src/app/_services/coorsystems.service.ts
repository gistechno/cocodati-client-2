import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { links } from '../../environments/environment';

@Injectable()
export class CoorsystemsService {

  constructor(private http:HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders()
  };
  
  getConfig():Observable<any>{
    return this.http.get(links.getConfig, this.httpOptions).pipe(map((res:Response) => res));
  }

  getTestCases():Observable<any>{
    return this.http.get(links.getTestcases, this.httpOptions).pipe(map((res:Response) => res));
  }

  getLanguageConfig(id:any):Observable<any>{
    return this.http.get(links.getLanguageConfig + id + '/getlanguages/', this.httpOptions).pipe(map((res:Response) => res));
  }

  convertFile(param:any, file:File):Observable<any>{
    let body:string = JSON.stringify(param);
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    formData.append('body', body);
  
    let params = new HttpParams();
    const options = {
      params: params,
      reportProgress: true,
    };
    return this.http.post(links.postFile, formData, options).pipe(map((res:Response) => res));
  }

  convert(param:any):Observable<any>{
    let body:string = JSON.stringify(param);
    let options = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(links.convertCoords, body,options).pipe(map((res:Response) => res));
  }
}
