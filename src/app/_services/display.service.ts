import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DisplayService {

  file_sent_open:boolean = false;

  constructor() { }

  getFileOpen(){
    return this.file_sent_open;
  }

  setFileOpen(status:boolean){
    this.file_sent_open = status;
  }
}
