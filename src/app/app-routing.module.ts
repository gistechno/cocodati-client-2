import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConverterComponent } from './converter/converter.component';
import { TestconversionsComponent } from './testconversions/testconversions.component';

const routes: Routes = [
    
  { path: '', component: ConverterComponent },
  { path: 'testing', component: TestconversionsComponent },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
