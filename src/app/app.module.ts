import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ConverterComponent } from './converter/converter.component';
import { AppRoutingModule } from './app-routing.module';
import {CoorsystemsService} from './_services/coorsystems.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TestconversionsComponent } from './testconversions/testconversions.component';

@NgModule({
  declarations: [
    AppComponent,
    ConverterComponent,
    TestconversionsComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [HttpClientModule,CoorsystemsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
