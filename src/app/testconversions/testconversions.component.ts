import { Component, OnInit } from '@angular/core';
import { CoorsystemsService } from '../_services/coorsystems.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-testconversions',
  templateUrl: './testconversions.component.html',
  styleUrls: ['./testconversions.component.css']
})
export class TestconversionsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private csService: CoorsystemsService) { }

  y_cord_converted:any = '';
  x_cord_converted:any = '';
  z_cord_converted:any = '';
  pan_cord_x:any = '';
  pan_cord_y:any = '';
  
  
  converted: any = [];
  cases:any = [];
  testcases:any = [];

  ngOnInit() {
    this.csService.getTestCases().subscribe(res => {
      this.testcases = res.cases
      for(let i in this.testcases){
        let temp = this.testcases[i];
        let req = [{
          system: temp.system,
          system_to:temp.system_to,
          from: temp.from,
          to: temp.to,
          y: temp.y,
          x: temp.x,
          z: temp.z,
          zone: temp.zone,
          expected_Y: temp.expected_Y,
          expected_X: temp.expected_X,
          expected_Z: temp.expected_Z,
          latlong_format: temp.latlong_format,
          height_from: temp.height_from,
          height_to:temp.height_to,
          gk_zone: temp.gk_zone,
          to_epoch: temp.to_epoch,
          from_epoch: temp.from_epoch
        }];
        
        this.csService.convert(req).subscribe(res => {
          let data = req[0];
          data["returnedY"] = res['y'];
          data["returnedX"] = res['x'];
          data["returnedZ"] = res['z'];
          data["results"] = [data.expected_Y == res['y'], data.expected_X==res['x'], data.expected_Z==res['z']];
          if(data["results"][0] == data["results"][1] == data["results"][2] == true){
            data["results"] = "Correct";
          }
          else{
            data["results"] = "Error/Does not match";
          }
          this.cases.push(data);
        })
      }
      
    });
    
  }

  convert(){
    this.cases = [];
    for(let i in this.testcases){
      let temp = this.testcases[i];
      let req = [{
        system: temp.system,
        system_to:temp.system_to,
        from: temp.from,
        to: temp.to,
        y: temp.y,
        x: temp.x,
        z: temp.z,
        zone: temp.zone,
        expected_Y: temp.expected_Y,
        expected_X: temp.expected_X,
        expected_Z: temp.expected_Z,
        height_from: temp.height_from,
        height_to:temp.height_to
      }];
      
      this.csService.convert(req).subscribe(res => {
        let data = req[0];
        data["returnedY"] = res['y'];
        data["returnedX"] = res['x'];
        data["returnedZ"] = res['z'];
        data["results"] = [data.expected_Y == res['y'], data.expected_X==res['x'], data.expected_Z==res['z']];
        this.cases.push(data);
      })
    }
    

    
  }

}
