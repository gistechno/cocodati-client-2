import { Component, OnInit } from '@angular/core';
import { CoorsystemsService } from '../_services/coorsystems.service';
import { DisplayService} from '../_services/display.service';
import { ActivatedRoute } from '@angular/router';
import { wmtslink } from '../../environments/environment';


import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


import Map from 'ol/Map';
import View from 'ol/View';
import WMTSCapabilities from 'ol/format/WMTSCapabilities.js';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import WMTS, {optionsFromCapabilities} from 'ol/source/WMTS.js'
import {toLonLat ,fromLonLat, transform} from 'ol/proj';
import { toStringHDMS } from 'ol/coordinate';
import Overlay from 'ol/Overlay';
import TileWMS from 'ol/source/TileWMS';

import {defaults as defaultControls, Control} from 'ol/control.js';

//import WMTSTileGrid from 'ol/tilegrid/WMTS.js';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: [
  './converter.component.css'
  ]
})
export class ConverterComponent implements OnInit {

  
  constructor(private route: ActivatedRoute, private csService: CoorsystemsService,private dS: DisplayService, private modalService: NgbModal) { }

  configs:any = [];

  language_configs:any = [];
  languages = [
    {id: 0, name:'icelandic'},
    {id: 1, name: 'english'}
  ];
  language_selected = this.languages[0];
  l:any = []; //short name for language, holds text in correct language

  flag_urls = [
    "assets/img/is_thumb33x24.png",
    "assets/img/british-flag_thumb.png"
  ]
  flag_selected = this.flag_urls[1];

  /**
   * TODO: This should really be in a config file
   */
  system_selected = 'ISN2016';
  system_to_selected = 'ISN2016';
  sub_systems_from:any = ['LatLong', 'LCC', 'UTM', 'GK', 'XYZ'];
  sub_system_from_selected = 'LCC';
  sub_systems_to:any = ['LatLong', 'LCC', 'UTM', 'GK', 'XYZ'];
  sub_system_to_selected = 'LatLong';
  systems:any = ['ISN93', 'ISN2004', 'ISN2016', 'Hjörsey', 'ISN_DRF', "Reykjavik1900"];
  systems_to:any = ['ISN93', 'ISN2004', 'ISN2016', 'Hjörsey'];
  heights = ['h', 'MSL'];
  heights_to = ['h', 'MSL'];
  height_from_selected =  'h';
  height_to_selected = 'h';
  to_epoch = "2016.5";
  from_epoch = "2018.619";

  drf_subsystems = ['LatLong', 'LCC','XYZ'];
  westOf = ['Greenwich', 'Copenhagen']
  westOf_selected = 'Greenwich';

  theights = [{dName:'h', cName:'h'},{dName:'ISH', cName:'MSL'} ];

  y_cord:any = '0';
  x_cord:any = '0';
  z_cord:any = '0';

  utm_zones:any = ['26','27','28'];
  zone:any = '26';
  gk_zones:any = [{dName:'4', cName:'15'},{dName:'3', cName:'18'},{dName:'2', cName:'21'} ,{dName:'1', cName:'24'}];
  gk_zone:any = '18';

  latlong_formats = ['DMS', 'DM', 'Decimal'];
  latlong_format_selected = 'DMS';

  y_cord_converted: any = '0';
  x_cord_converted: any = '0';
  z_cord_converted: any = '0';

  y_from_cord_name:any = 'y';
  x_from_cord_name:any = 'x';
  z_from_cord_name:any = 'z';

  y_to_cord_name:any = 'y';
  x_to_cord_name:any = 'x';
  z_to_cord_name:any = 'z';

  pan_cord_x: any = -20.0;
  pan_cord_y: any = 65.0;
  
  is_file_conversion: boolean = false;

  email:string = '';
  username:string = '';
  fileToUpload: File = null;
  file_header:string = '0';

  closeResult: string;

  inputInfo: string = '';
  instructions: string = '';

  filesent_status = 'No file sent';

  pop_up_message = {
    header: " ",
    message: " "
  }


  ngOnInit() {
    this.updateLanguage();
    this.csService.getConfig().subscribe(configs => {
      this.configs = configs;
      this.initSystems();
    });

    this.updatePopUpMessage();
    

    var parser = new WMTSCapabilities();

    fetch(wmtslink).then((response) => {
      return response.text();
    }).then((text) => {
      var result = parser.read(text);
      var iceland = [-19,65]
      var icelandWebMercator = fromLonLat(iceland);
      
      //causes error
      var view = new View({
        center: icelandWebMercator,
        zoom: 5
      });

      //Elements that make up the popup.
      var container = document.getElementById('popup');
      var content = document.getElementById('popup-content');
      var closer = document.getElementById('popup-closer');

      //Create an overlay to anchor the popup to the map.      
      var overlay = new Overlay({
        element: container,
        autoPan: true,
        autoPanAnimation: {
          duration: 250
        }
      });

      /**
      * Add a click handler to hide the popup.
      * @return {boolean} Don't follow the href.
      */
      closer.onclick = () => {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
      };
      
    
      var layers = [
        new TileLayer({
          opacity: 1,
          source: new TileWMS({
            url: 'https://gis.lmi.is/mapcache/web-mercator/wms',
            params: {'LAYERS': 'LMI_Kort', 'TILED': true, 'FORMAT':'image/png'},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
          })
        }),
        new TileLayer({
          opacity: 1,
          source: new TileWMS({
            url: 'https://gis.lmi.is/geoserver/wms',
            params: {'LAYERS': 'Ornefni', 'TILED': true, 'FORMAT':'image/png'},
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
          })
        }),
        new TileLayer({
          source: new OSM(),
          visibility:false
        })
      ]
  
      layers[2].setVisible(false);

      var map = new Map({
        target: 'map',
        layers: layers,
        overlays: [overlay],
        // Improve user experience by loading tiles while animating. Will make
        // animations stutter on mobile or slow devices.
        loadTilesWhileAnimating: true,
        view: view//,
        //controls:[]
      });

      map.on('moveend',function(e){
        var zoomLevel = map.getView().getZoom();
        if(zoomLevel > 13){
          layers[0].setVisible(false);
          layers[2].setVisible(true);
        }
        else{
          layers[0].setVisible(true);
          layers[2].setVisible(false);
        }
        
      });
      
      map.on('click', (evt) => {
        var coordinate = evt.coordinate
        var hdms = transform(
          coordinate, 'EPSG:3857', 'EPSG:4326');
        
        var tempHdms = hdms;
        
        var hdms = toStringHDMS(toLonLat(coordinate));

        if(this.x_cord == '0' && this.y_cord == '0' && this.sub_system_from_selected == "LatLong"){
          var tempHdms = hdms.split("N")
          this.x_cord = tempHdms[1].substring(1); 
          this.y_cord = tempHdms[0] + "N"
        }
        content.innerHTML = '<div class="code-text">' + hdms + '</div>';
        overlay.setPosition(coordinate);
        
      });   

      function onClick(id, callback) {
        document.getElementById(id).addEventListener('click', callback);
      }
      var pan_to = fromLonLat([-0.12755, 51.507222]);
      
      onClick('pan-to', () => {
        if(this.is_file_conversion){
          //this.convertFile();
        }
        else{
          if(this.sub_system_to_selected == 'XYZ'){
            this.height_to_selected = 'h';
          }
          if(this.sub_system_from_selected == 'XYZ'){
            this.height_from_selected = 'h';
          }
          if(this.system_selected == 'Hjörsey' || this.system_to_selected == 'Hjörsey'){
            if(this.sub_system_from_selected != 'XYZ'){
              this.z_cord = '0';
            }
            this.height_from_selected = 'h';
            this.height_to_selected = 'h';
          }
          if(this.system_selected == 'ISN_DRF'){
            this.height_from_selected = 'h';
          }
          if(this.system_selected == 'Reykjavik1900' && this.sub_system_from_selected == 'LatLong' && this.westOf_selected == 'Copenhagen'){
            this.sub_system_from_selected = 'LatLongCPH';
          }
          if(this.system_to_selected == 'Reykjavík' && this.sub_system_from_selected == 'LatLong'){
            if(this.y_cord < 64.01 || this.y_cord > 64.21 || this.x_cord < -22.06 || this.x_cord > -21.54){
              this.system_to_selected = 'ISN2016';
              this.sub_system_to_selected = 'LCC';
            }
          }
          let req = [{
            system: this.system_selected,
            system_to: this.system_to_selected,
            from: this.sub_system_from_selected,
            to: this.sub_system_to_selected,
            y: this.y_cord,
            x: this.x_cord,
            z: this.z_cord,
            zone: this.zone,
            gk_zone: this.gk_zone,
            latlong_format: this.latlong_format_selected,
            height_from:this.height_from_selected,
            height_to: this.height_to_selected,
            to_epoch: this.to_epoch,
            from_epoch:this.from_epoch
          }];
      
          this.csService.convert(req).subscribe(res => {
            console.log(res);
            this.y_cord_converted = res['y'];
            this.x_cord_converted = res['x'];
            this.z_cord_converted = res['z'];
            this.pan_cord_x = res['latlongX'];
            this.pan_cord_y = res['latlongY'];
            
            pan_to = fromLonLat([this.pan_cord_x, this.pan_cord_y])
            view.animate({
              center: pan_to,
              duration: 2000
            });
            var coordinate = pan_to;
            var hdms = toStringHDMS(toLonLat(coordinate));
            content.innerHTML = '<p>' + hdms + '</p>';
            //content.innerHTML = hdms;
            overlay.setPosition(coordinate);

            if(this.sub_system_from_selected == 'LatLongCPH'){
              this.sub_system_from_selected = 'LatLong';
            }
          });
        }
      });
    });

    
  }

  handleFileInput(files: FileList) {
    this.systems_to
    this.fileToUpload = files.item(0);
  }


  convertFile(){
    if(this.sub_system_to_selected == 'XYZ'){
      this.height_to_selected = 'h';
    }
    if(this.sub_system_from_selected == 'XYZ'){
      this.height_from_selected = 'h';
    }
    if(this.system_selected == 'Hjörsey' || this.system_to_selected == 'Hjörsey' || this.system_selected == 'Reykjavik1900' || this.system_to_selected == 'Reykjavik1900'){
      if(this.sub_system_from_selected != 'XYZ'){
        this.z_cord = '0';
      }
      this.height_from_selected = 'h';
      this.height_to_selected = 'h';
    }
    if(this.system_selected == 'Reykjavik1900' && this.sub_system_from_selected == 'LatLong' && this.westOf_selected == 'Copenhagen'){
      this.sub_system_from_selected = 'LatLongCPH';
    }
    if(this.system_selected == 'ISN_DRF'){
      this.height_from_selected = 'h';
    }
    let req = {
      system: this.system_selected,
      system_to: this.system_to_selected,
      from: this.sub_system_from_selected,
      to: this.sub_system_to_selected,
      lines: this.file_header,
      email:this.email,
      username: this.username,
      utm_zone:this.zone,
      gk_zone:this.gk_zone,
      language: this.language_selected.name,
      height_from:this.height_from_selected,
      height_to: this.height_to_selected,
      latlong_format: this.latlong_format_selected,
      to_epoch: this.to_epoch,
      from_epoch:this.from_epoch,
    }
    if(this.email == "" || this.username == ""){
      this.updatePopUpMessage();
    }
    else{
      this.csService.convertFile(req,this.fileToUpload).subscribe(configs => {
        console.log("file sent");
        this.filesent_status = configs['status'];
        console.log(this.filesent_status);
        this.updatePopUpMessage();
      });
    }
    if(this.sub_system_from_selected == 'LatLongCPH'){
      this.sub_system_from_selected = 'LatLong';
    }
    
  
  }

  clickHeightRadio(){
    this.initSystems();
    this.updateCoordNames();
  }

  initSystems(){
    if(this.system_selected == 'ISN_DRF' && this.system_to_selected != 'ISN2016'){
      this.system_to_selected = 'ISN_DRF';
    }
    else if(this.system_selected == 'Reykjavík' ){
      if(this.system_to_selected != "ISN2016" && this.system_to_selected != "ISN93"){
        this.system_to_selected = 'ISN2016';
      }
      this.sub_system_from_selected = 'XY';
    }
    /*
    if(this.system_to_selected == 'ISN_DRF' && this.system_selected != 'ISN_DRF'){
      this.system_to_selected = this.system_selected;
  }*/
    else if(this.system_selected == "Reykjavik1900" && this.system_to_selected != 'Reykjavik1900' && this.system_to_selected != 'Hjörsey' && this.system_to_selected != 'ISN93'){
      this.system_to_selected = 'Reykjavik1900';
    }
    else if(this.system_selected == 'ISN93' && this.system_to_selected == "Reykjavik1900"){
      //console.log("aha");
    }
    else if(this.system_selected == 'ISN2016' && this.system_to_selected == 'ISN_DRF'){
      //console.log("aha");
    }
    else if(this.system_selected == 'Reykjavik1900' && (this.system_to_selected == 'Reykjavik1900' || this.system_to_selected == 'Hjörsey')){
      //console.log("aha");
    }
    else if(this.system_selected == 'Hjörsey' && this.system_to_selected =="Reykjavik1900"){
      //console.log("aha");
    }
    else if(this.system_to_selected == 'Reykjavík' && this.sub_system_from_selected != 'LCC' && this.sub_system_from_selected != 'LatLong'){
      this.system_to_selected = 'ISN2016';
      this.sub_system_to_selected = 'LCC';
    }
    else if(!this.systems_to.includes(this.system_to_selected)){
      if(this.system_to_selected != "Reykjavík"){
        this.system_to_selected = this.systems_to[0];
      }
      
    }
    if(this.system_to_selected == 'Reykjavík'){
      this.sub_system_to_selected = 'XY';
    }
    if(this.system_selected != 'Reykjavík' && this.sub_system_from_selected == 'XY'){
      this.sub_system_from_selected = "LCC";
    }
    this.updateInformation();
    this.updateCoordNames();
  }

  updatePopUpMessage(){
    if(this.filesent_status == 'Command not available'){
      this.pop_up_message.header = this.l['Error'];
      this.pop_up_message.message = this.l['filesent_error_message'];
    }
    else if(this.filesent_status == 'Bad File'){
      this.pop_up_message.header = this.l['Error'];
      this.pop_up_message.message = this.l['filesent_file_error'];
    }
    else if(this.email == "" || this.username == ""){
      this.pop_up_message.header = this.l['Error'];
      this.pop_up_message.message = this.l['filesent_missing_input'];
    }
    else{
      this.pop_up_message.header = this.l['filesent'];
      this.pop_up_message.message = this.l['filesent_message'];
    }
    
  }

  updateInformation(){
    if(this.sub_system_from_selected == 'LCC'){
      this.inputInfo = this.l["ex"][this.sub_system_from_selected][this.system_selected];
    }
    else{
      this.inputInfo = this.l["ex"][this.sub_system_from_selected];
    }
    
  }

  updateCoordNames(){
    if(this.system_selected == 'Hjörsey' && this.sub_system_from_selected == 'GK'){
      this.y_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['x'];
      this.x_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['y'];
    }
    else{
      this.y_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['y'];
      this.x_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['x'];
    }
    if(this.system_to_selected == 'Hjörsey' && this.sub_system_to_selected == 'GK'){
      this.x_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['y'];
      this.y_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['x'];
    }
    else{
      this.y_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['y'];
      this.x_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['x'];
    }
    this.z_from_cord_name = this.l['coordnames'][this.height_from_selected][this.sub_system_from_selected]['z'];
    this.z_to_cord_name = this.l['coordnames'][this.height_to_selected][this.sub_system_to_selected]['z'];
  }

  updateLanguage(){
    this.csService.getLanguageConfig(this.language_selected.id).subscribe(languages => {
      this.l = languages;
      this.instructions = this.l['ex']['Instructions'];
      if(this.sub_system_from_selected == 'LCC'){
        this.inputInfo = this.l["ex"][this.sub_system_from_selected][this.system_selected];
      }
      else{
        this.inputInfo = this.l["ex"][this.sub_system_from_selected];
      }
      this.updateCoordNames();
    });
  }

  changeLanguage(){
    if(this.language_selected.id == 1){
      this.language_selected = this.languages[0];
      this.flag_selected = this.flag_urls[1];
    }
    else{
      this.language_selected = this.languages[1];
      this.flag_selected = this.flag_urls[0];
    }
    this.updateLanguage();
    this.updateCoordNames();
  }

  fileDp(){
    return this.dS.getFileOpen();
  }
  closeFileBox(){
    this.dS.setFileOpen(false);
  }
  openFileBox(){
    this.dS.setFileOpen(true);
  }

  fileConvert(){
    this.openFileBox();
    if(this.is_file_conversion){
      this.convertFile();
    }
  }

}
